# COMPILE
# "pyinstaller -F -n Game+ --distpath . --clean game+.py"


import winreg
import argparse

def set_cpu_priority_class(process_name):
    try:
        # Création de la clé principale
        registry_key = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE, 
                                         "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution Options\\" + process_name)

        # Création de la sous-clé 'PerfOptions'
        perf_options_key = winreg.CreateKey(registry_key, 'PerfOptions')

        # Création de la valeur DWORD 'CpuPriorityClass' avec la valeur 3
        winreg.SetValueEx(perf_options_key, 'CpuPriorityClass', 0, winreg.REG_DWORD, 3)

        # Fermeture des clés
        winreg.CloseKey(perf_options_key)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False

def main():
    # Parse les arguments de la ligne de commande
    parser = argparse.ArgumentParser(description='Set the CPU priority class of a process.')
    parser.add_argument('process', type=str, help='the name of the process')
    args = parser.parse_args()

    # Appelle la fonction avec le nom du processus en argument
    set_cpu_priority_class(args.process)

if __name__ == '__main__':
    main()
